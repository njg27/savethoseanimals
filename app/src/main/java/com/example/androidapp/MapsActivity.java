package com.example.androidapp;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    public static final String TAG = "MapsActivity";
    private static final float DEFAULT_ZOOM = 15f;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0;
    private String DEFAULT_PHONE_NUMBER = "87480897"; //INPUT HQ OR RELATED DEPARTMENT'S NUMBER TO RESPONSE TO SOS
    private GoogleMap mMap;
    private EditText mSearchText;
    private ImageView mGPS;
    private ImageView mZOOM;
    private ImageView mPET;
    private ImageView mAlert;
    private ImageView mSOS;
    private static final int REQUEST_CODE = 101;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private UserLocation mUserLocation;
    FusedLocationProviderClient mFusedLocationProviderClient;
    Location mCurrentLocation;

    private LocationManager mLocationManager;
    private android.location.LocationListener mLocationListener;
    private MarkerOptions mMarkerOptions;
    private LatLng mOrigin;
    private LatLng mDestination;
    private Polyline mPolyline;
    ArrayList<LatLng> mMarkerPoints;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mSearchText = findViewById(R.id.input_search);
        mGPS = findViewById(R.id.ic_gps);
        mZOOM = findViewById(R.id.ic_zoom);
        mPET = findViewById(R.id.ic_pet);
        mAlert = findViewById(R.id.ic_alert);
        mSOS = findViewById(R.id.ic_sos);
        db = FirebaseFirestore.getInstance();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mMarkerPoints = new ArrayList<>();

        getLastLocation();



    }

    //Step 3: save user data - firestore db =======================================================================================================================================
    private void saveUserLocationData(){
        Log.d(TAG, "SULD CALLEDDDDD");

        DocumentReference locationReference = db.collection("User Locations").document(FirebaseAuth.getInstance().getCurrentUser().getUid());
        locationReference.set(mUserLocation).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d("SAVE USER LOCATION", "Users location SAVED");

                }
            }
        });

    }

    // Step 2: get user location - firestore db
    private void getLastKnownLocation(){
        Log.d(TAG, "OnComplete: GLKL IS CALLED");
        GeoPoint geoPoint = new GeoPoint(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        mUserLocation.setGeoPoint(geoPoint);
        mUserLocation.setTimestamp(null);
        saveUserLocationData();
        startLocationService();




    }

    //step 1: get user data - firestore db
    private void getUserData(){
        if(mUserLocation == null){
            mUserLocation = new UserLocation();
            Log.d("getUserData()","NOTE!!!!!!!!!!!OnComplete:"+ mUserLocation.toString());
            DocumentReference documentReference = db.collection("Users").document(FirebaseAuth.getInstance().getCurrentUser().getUid() );

            documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        Log.d("USER DATA:","User data OBTAINED");
                        User user = task.getResult().toObject(User.class);
                        mUserLocation.setUser(user);
                        ((UserClient)getApplicationContext()).setUser(user);
                        getLastKnownLocation();
                    }
                }
            });


        }
    }
    //=======================================================================================================================================================================

    private void init(){ //Function created to set all image assets' functions
        Log.d(TAG, "Init : Initialising");

        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) { //Search places on google map e.g. malaysia
                if(actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event.getAction() == event.ACTION_DOWN
                        || event.getAction() == event.KEYCODE_ENTER){

                    //Execute method for searching
                    geoLocate();

                }
                return false;
            }
        });

        mGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  //Set marker on current device location
                getDeviceLocation();
                double latLng1 = mCurrentLocation.getLatitude();
                double latLng2 = mCurrentLocation.getLongitude();
                GeoPoint coordinates = new GeoPoint(latLng1, latLng2);
                Drawable circleDrawable = getDrawable(R.drawable.ic_pet);
                BitmapDescriptor icon = getMarkerIconFromDrawable(circleDrawable);
                MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(latLng1, latLng2)).icon(icon);
                mMap.addMarker(markerOptions);
                Log.d("Marker adding", "Marker is added");

                CollectionReference dbLat = db.collection("Coordinates");

                Coordinates geoPoint = new Coordinates(coordinates); //Created POJO class, "Coordinates" for this instance
                //Created new object to parse in data to be sent to FireStore


                dbLat.add(geoPoint).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) { //Add data to FireStore and add listener to test if it is successful
                        Toast.makeText(MapsActivity.this, "Marker created", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MapsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });






            }
        });

        mZOOM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //Shift camera to current location
                moveCamera(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), DEFAULT_ZOOM);
            }
        });

        mPET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //LogOut button located at the top right of the app
                try {
                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(MapsActivity.this, MainActivity.class));
                    // signed out
                } catch (Error error){
                    // an error
                }

            }
        });

        mAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //Get the updated markers from FireStore and reset it on the map
                db.collection("Coordinates")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Log.d(TAG, document.getId() + " => " + document.getData());
                                        GeoPoint geo = document.getGeoPoint("geoPoint");
                                        double lat = geo.getLatitude();
                                        double lng = geo.getLongitude();
                                        Drawable circleDrawable = getDrawable(R.drawable.ic_pet);
                                        BitmapDescriptor icon = getMarkerIconFromDrawable(circleDrawable);
                                        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lng)).icon(icon);
                                        mMap.addMarker(markerOptions);
                                        getUserData();
                                    }
                                } else {
                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                }
                            }
                        });
            }
        });

        mSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // SMS clickable image asset to call for HELP/ Send updates/ Contact Admin. UserId will be sent together with add-on comments if this image asset is clicked
                sendSMSMessage();
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.putExtra("address", DEFAULT_PHONE_NUMBER);
                sendIntent.putExtra("sms_body", "user_id: "+ FirebaseAuth.getInstance().getCurrentUser().getUid());
                sendIntent.setType("vnd.android-dir/mms-sms");
                startActivity(sendIntent);
                Toast.makeText(getApplicationContext(), "Help will be sent after clicking send.",
                        Toast.LENGTH_LONG).show();

            }
        });

    } //===================================================================================================================================================================

    protected void sendSMSMessage() { //SMS function
        if (ContextCompat.checkSelfPermission(this, //Check if permission is allowed
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this,  // Else request for permission from user
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
    }

    private void startLocationService(){
        if(!isLocationServiceRunning()){
            Intent serviceIntent = new Intent(this, GpsService.class); // Creating service intent

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){   //api 26 or abv

                MapsActivity.this.startForegroundService(serviceIntent);
            }else{
                startService(serviceIntent); //if api25 or lower
            }
        }
    }

    private boolean isLocationServiceRunning() { //This method is to run a test to check if my service is running in the background
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if("com.example.androidapp.services.GpsService".equals(service.service.getClassName())) {
                Log.d(TAG, "isLocationServiceRunning: location service is already running.");
                return true;
            }
        }
        Log.d(TAG, "isLocationServiceRunning: location service is not running.");
        return false;
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }




    private void geoLocate(){ //This method is for map browsing/searching through the search bar
        Log.d(TAG, "geoLocate: geolocating");
        String search = mSearchText.getText().toString();
        Geocoder geocoder = new Geocoder(MapsActivity.this);
        List<Address> list = new ArrayList<>();
        try{
            list = geocoder.getFromLocationName(search, 1);

        }catch (IOException e){
            Log.e(TAG, "geoLocate: IOException: " + e.getMessage());
        }
        if(list.size() > 0){
            Address address = list.get(0);

            Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();
            LatLng coordinate = new LatLng(address.getLatitude(),address.getLongitude());

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, DEFAULT_ZOOM));
        }
    }

    private void getDeviceLocation(){ //This is callable after getlastlocation get user's permission
        //Method called when clicking on the set marker icon. To move camera focus and plant marker on current device location
        Log.d(TAG, "getDeviceLocation: retrieving current location");
        Task location = mFusedLocationProviderClient.getLastLocation();
        location.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()){
                    Log.d(TAG, "OnComplete: Found Location");
                    Location location1 = (Location) task.getResult();
                    moveCamera(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), DEFAULT_ZOOM);
                    getUserData();
                }
                else{
                    Log.d(TAG, "OnComplete: Location not found");
                    Toast.makeText(MapsActivity.this, "Unable to find location", Toast.LENGTH_SHORT).show();
                }
            }
        });
        getUserData();

    }

    private void moveCamera(LatLng latLng, float zoom){ //Overwrite the method "moveCamera" under "CameraUpdateFactory" class in android
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void getLastLocation() { // check and get user's permission to allow location service on phone.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }

        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    mCurrentLocation = location;
                    Toast.makeText(getApplicationContext(),mCurrentLocation.getLatitude() +"  "+ mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    SupportMapFragment mSupportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
                    mSupportMapFragment.getMapAsync(MapsActivity.this);
                }
            }
        });
        getUserData();
    }



    private void getMyLocation(){
        // Getting LocationManager object from System Service LOCATION_SERVICE
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mOrigin = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mOrigin,12));
                //if(mOrigin != null && mDestination != null)
                //drawRoute();
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                //Required class but not using. Hence return null.
            }

            public void onProviderEnabled(String provider) {
                //Required class but not using. Hence return null.
            }

            public void onProviderDisabled(String provider) {
                //Required class but not using. Hence return null.
            }
        };

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.M) {  //Check current API if 26 n abv
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED) {
                mMap.setMyLocationEnabled(true);
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, mLocationListener);
                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        mDestination = latLng;
                        mMap.clear();
                        mMarkerOptions = new MarkerOptions().position(mDestination).title("Destination");
                        mMap.addMarker(mMarkerOptions);
                        if (mOrigin != null && mDestination != null)
                            Log.d("drawRoute", "OnComplete: DrawRoute called");
                        drawRoute();

                        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                                builder.setMessage("Open Google Maps?") //Obtain YES/NO from user as a popup msg appear on screen
                                        .setCancelable(true)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) { //If user choose yes, map will open google map on the device and parse in coordinates to navigate
                                                String latitude = String.valueOf(mDestination.latitude);
                                                String longitude = String.valueOf(mDestination.longitude);
                                                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
                                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                                mapIntent.setPackage("com.google.android.apps.maps");

                                                try{ //If user does have google map in the phone after choosing yes
                                                    if (mapIntent.resolveActivity(MapsActivity.this.getPackageManager()) != null) {
                                                        startActivity(mapIntent);
                                                    }
                                                }catch (NullPointerException e){ //If user do not have google maps downloaded in the phone, this catch will prevent app from closing.
                                                    Log.e(TAG, "onClick: NullPointerException: Couldn't open map." + e.getMessage() );
                                                    Toast.makeText(MapsActivity.this, "Couldn't open map", Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                                dialog.cancel();
                                            }
                                        });
                                final AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });


                    }

                });
            }


        }
    }
    // ===============================================Draw routes between destination and current location=================================================================
    private void drawRoute(){

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(mOrigin, mDestination);
        DownloadTask downloadTask = new DownloadTask();
        Log.d("DrawRoute","GETTING DIRECTION");


        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    private String getDirectionsUrl(LatLng mOrigin,LatLng mDestination){

        // Origin of route
        String str_origin = "origin="+mOrigin.latitude+", "+mOrigin.longitude;

        // Destination of route
        String str_dest = "destination="+mDestination.latitude+","+mDestination.longitude;

        // Key
        String key = "key=" + getString(R.string.google_maps_key);

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception on download", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("DownloadTask","DownloadTask : " + data);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Directions in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                JSONParser parser = new JSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            //ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                mMarkerPoints = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching n-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    mMarkerPoints.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(mMarkerPoints);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if(mPolyline != null){
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);

            }else
                Toast.makeText(getApplicationContext(),"No route is found", Toast.LENGTH_LONG).show();
        }
    }
    // ======================================================================================================================================================================================
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        init();
        LatLng latLng = new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
        MarkerOptions mMarkerOptions = new MarkerOptions().position(latLng).title("You are here");

        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,DEFAULT_ZOOM));
        //mMap.addMarker(mMarkerOptions);

        db.collection("Coordinates")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                GeoPoint geo = document.getGeoPoint("geoPoint");
                                double lat = geo.getLatitude();
                                double lng = geo.getLongitude();
                                Drawable circleDrawable = getDrawable(R.drawable.ic_pet);
                                BitmapDescriptor icon = getMarkerIconFromDrawable(circleDrawable);
                                MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lng)).icon(icon);
                                mMap.addMarker(markerOptions);
                                getUserData();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        getMyLocation();


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getLastLocation();
                    getUserData();
                }
                break;
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

}





