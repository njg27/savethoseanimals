package com.example.androidapp;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class UserLocation {
    private GeoPoint geoPoint;
    private @ServerTimestamp Date timestamp;
    private User user;

    public UserLocation() {
    }

    public UserLocation(GeoPoint geoPoint, Date timestamp, User user) {
        this.geoPoint = geoPoint;
        this.timestamp = timestamp;
        this.user = user;
    }

    public GeoPoint getGeoPoint() {

        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public Date getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void getData(){
        return ;
    }

    @Override //For Logcat
    public String toString() {
        return "UserLocation{" +
                "users=" + geoPoint +
                ", timestamp=" + timestamp +
                ", user=" + user +
                '}';
    }


}
