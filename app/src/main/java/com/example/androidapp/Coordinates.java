package com.example.androidapp;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.GeoPoint;

public class Coordinates {
    private GeoPoint geoPoint;


    public Coordinates(){

    }

    public Coordinates(GeoPoint geoPoint) {

        this.geoPoint = geoPoint;

    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }
}
