package com.example.androidapp;



public class UserList {
    private String userID;
    private String email;

    public UserList() {
    }

    public UserList(String userID, String email) {
        this.userID = userID;
        this.email = email;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setData(String userID, String email){
        this.userID = userID;
        this.email = email;
    }
}
