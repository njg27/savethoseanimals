package com.example.androidapp;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserListTest {

    private UserList userList= new UserList("abcd0000","abcd@gmail.com");

    @Test
    public void getUserID() {
        assertEquals("abcd0000",userList.getUserID());
    }

    @Test
    public void getEmail() {
        assertEquals("abcd@gmail.com",userList.getEmail());
    }
}